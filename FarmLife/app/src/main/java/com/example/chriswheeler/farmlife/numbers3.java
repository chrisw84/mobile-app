package com.example.chriswheeler.farmlife;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class numbers3 extends AppCompatActivity {

    EditText userAnswer3;
    String strInput3 = "";
    Button answer3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numbers3);

        // Setting EditText and Button within onCreate
        Button answer3 = (Button) findViewById(R.id.answer3);
        final EditText userAnswer3 = (EditText) findViewById(R.id.userAnswer3);

        // setting instructions for when button is clicked.
        answer3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // instruction to get number from the EditText
                String check = userAnswer3.getText().toString();

                // If number within the EditText matches the below, complete these instructions
                // Code based on answer at http://stackoverflow.com/questions/34095450/compare-edittext-value-with-string
                if(check.equalsIgnoreCase("7"))
                {
                    strInput3 = strInput3+","+check;
                    Toast toast = Toast.makeText(getApplicationContext(), "Correct", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpNumbers3 = MediaPlayer.create(numbers3.this, R.raw.correct);
                    mpNumbers3.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpNumbers3.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpNumbers3.release();
                        }
                    });
                    userAnswer3.setText(strInput3);
                    userAnswer3.setText("");
                    RelativeLayout rlNumbers3 = (RelativeLayout) findViewById(R.id.RLnumbers3);
                    rlNumbers3.setBackgroundColor(Color.GREEN);

                    // Timer to wait until above instructions are executed until activity changes.
                    TimerTask numbers3TT = new TimerTask() {
                        @Override
                        public void run() {

                            Intent numbers4 = new Intent(numbers3.this, numbers4.class);
                            startActivity(numbers4);

                        }
                    };

                    Timer numbers3Timer = new Timer();
                    numbers3Timer.schedule(numbers3TT, 3000);

                }

                // If number within the EditText does not match, complete these instructions
                else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Not Quite. Try Again.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpNumbers3_1 = MediaPlayer.create(numbers3.this, R.raw.wrong);
                    mpNumbers3_1.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpNumbers3_1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpNumbers3_1.release();
                        }
                    });
                    userAnswer3.setText("");
                    RelativeLayout rlNumbers3 = (RelativeLayout) findViewById(R.id.RLnumbers3);
                    rlNumbers3.setBackgroundColor(Color.RED);
                }

                // Log to confirm button has been pressed
                Log.i("answer3", "Number Answer3 Button Pressed");
            }

        });

        // Link to take user back to home activity
        ImageButton pageLinkhome = (ImageButton) findViewById(R.id.pageLinkhome);
        pageLinkhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(numbers3.this, home.class);
                startActivity(home);

                // Log to confirm button has been pressed
                Log.i("pageLinkhome", "Number 3 Home Button Pressed");
            }
        });
    }
}
