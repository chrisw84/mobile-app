package com.example.chriswheeler.farmlife;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class numbers9 extends AppCompatActivity {

    EditText userAnswer9;
    String strInput9 = "";
    Button answer9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numbers9);

        // Setting EditText and Button within onCreate
        Button answer9 = (Button) findViewById(R.id.answer9);
        final EditText userAnswer9 = (EditText) findViewById(R.id.userAnswer9);

        // setting instructions for when button is clicked.
        answer9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // instruction to get number from the EditText
                String check = userAnswer9.getText().toString();

                // If number within the EditText matches the below, complete these instructions
                // Code based on answer at http://stackoverflow.com/questions/99099990/compare-edittext-value-with-string
                if(check.equalsIgnoreCase("12"))
                {
                    strInput9 = strInput9+","+check;
                    Toast toast = Toast.makeText(getApplicationContext(), "Correct", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpNumbers9 = MediaPlayer.create(numbers9.this, R.raw.correct);
                    mpNumbers9.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpNumbers9.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpNumbers9.release();
                        }
                    });
                    userAnswer9.setText(strInput9);
                    userAnswer9.setText("");
                    RelativeLayout rlNumbers9 = (RelativeLayout) findViewById(R.id.RLnumbers9);
                    rlNumbers9.setBackgroundColor(Color.GREEN);

                    // Timer to wait until above instructions are executed until activity changes.
                    TimerTask numbers9TT = new TimerTask() {
                        @Override
                        public void run() {

                            Intent home = new Intent(numbers9.this, home.class);
                            startActivity(home);

                        }
                    };

                    Timer numbers9Timer = new Timer();
                    numbers9Timer.schedule(numbers9TT, 3000);

                }

                // If number within the EditText does not match, complete these instructions
                else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Not Quite. Try Again.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpNumbers9_1 = MediaPlayer.create(numbers9.this, R.raw.wrong);
                    mpNumbers9_1.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpNumbers9_1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpNumbers9_1.release();
                        }
                    });
                    userAnswer9.setText("");
                    RelativeLayout rlNumbers9 = (RelativeLayout) findViewById(R.id.RLnumbers9);
                    rlNumbers9.setBackgroundColor(Color.RED);
                }

                // Log to confirm button has been pressed
                Log.i("answer9", "Number Answer9 Button Pressed");
            }

        });

        // Link to take user back to home activity
        ImageButton pageLinkhome = (ImageButton) findViewById(R.id.pageLinkhome);
        pageLinkhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(numbers9.this, home.class);
                startActivity(home);

                // Log to confirm button has been pressed
                Log.i("pageLinkhome", "Number 9 Home Button Pressed");
            }
        });
    }
}
