package com.example.chriswheeler.farmlife;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class numbers8 extends AppCompatActivity {

    EditText userAnswer8;
    String strInput8 = "";
    Button answer8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numbers8);

        // Setting EditText and Button within onCreate
        Button answer8 = (Button) findViewById(R.id.answer8);
        final EditText userAnswer8 = (EditText) findViewById(R.id.userAnswer8);

        // setting instructions for when button is clicked.
        answer8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // instruction to get number from the EditText
                String check = userAnswer8.getText().toString();

                // If number within the EditText matches the below, complete these instructions
                // Code based on answer at http://stackoverflow.com/questions/88098880/compare-edittext-value-with-string
                if(check.equalsIgnoreCase("1"))
                {
                    strInput8 = strInput8+","+check;
                    Toast toast = Toast.makeText(getApplicationContext(), "Correct", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpNumbers8 = MediaPlayer.create(numbers8.this, R.raw.correct);
                    mpNumbers8.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpNumbers8.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpNumbers8.release();
                        }
                    });
                    userAnswer8.setText(strInput8);
                    userAnswer8.setText("");
                    RelativeLayout rlNumbers8 = (RelativeLayout) findViewById(R.id.RLnumbers8);
                    rlNumbers8.setBackgroundColor(Color.GREEN);

                    // Timer to wait until above instructions are executed until activity changes.
                    TimerTask numbers8TT = new TimerTask() {
                        @Override
                        public void run() {

                            Intent numbers9 = new Intent(numbers8.this, numbers9.class);
                            startActivity(numbers9);

                        }
                    };

                    Timer numbers8Timer = new Timer();
                    numbers8Timer.schedule(numbers8TT, 3000);

                }

                // If number within the EditText does not match, complete these instructions
                else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Not Quite. Try Again.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpNumbers8_1 = MediaPlayer.create(numbers8.this, R.raw.wrong);
                    mpNumbers8_1.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpNumbers8_1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpNumbers8_1.release();
                        }
                    });
                    userAnswer8.setText("");
                    RelativeLayout rlNumbers8 = (RelativeLayout) findViewById(R.id.RLnumbers8);
                    rlNumbers8.setBackgroundColor(Color.RED);
                }

                // Log to confirm button has been pressed
                Log.i("answer8", "Number Answer8 Button Pressed");
            }

        });

        // Link to take user back to home activity
        ImageButton pageLinkhome = (ImageButton) findViewById(R.id.pageLinkhome);
        pageLinkhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(numbers8.this, home.class);
                startActivity(home);

                // Log to confirm button has been pressed
                Log.i("pageLinkhome", "Number 8 Home Button Pressed");
            }
        });
    }
}
