package com.example.chriswheeler.farmlife;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class words6 extends AppCompatActivity {

    EditText wordUser6;
    String strInputwords6 = "";
    Button wordAnswer6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_words6);

        // Setting EditText and Button within onCreate
        Button answer6 = (Button) findViewById(R.id.wordAnswer6);
        final EditText wordUser6 = (EditText) findViewById(R.id.wordUser6);

        // setting instructions for when button is clicked.
        answer6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // instruction to get text from the EditText
                String check = wordUser6.getText().toString();

                // If text within the EditText matches the below, complete these instructions
                // Code based on answer at http://stackoverflow.com/questions/26096660/compare-edittext-value-with-string
                if (check.equalsIgnoreCase("lion")) {
                    strInputwords6 = strInputwords6 + "," + check;
                    Toast toast = Toast.makeText(getApplicationContext(), "Correct", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpWords6 = MediaPlayer.create(words6.this, R.raw.correct);
                    mpWords6.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpWords6.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpWords6.release();
                        }
                    });
                    wordUser6.setText(strInputwords6);
                    wordUser6.setText("");
                    RelativeLayout rlWords6 = (RelativeLayout) findViewById(R.id.RLwords6);
                    rlWords6.setBackgroundColor(Color.GREEN);

                    // Timer to wait until above instructions are executed until activity changes.
                    TimerTask words6TT = new TimerTask() {
                        @Override
                        public void run() {

                            Intent words7 = new Intent(words6.this, words7.class);
                            startActivity(words7);

                        }
                    };

                    Timer words6Timer = new Timer();
                    words6Timer.schedule(words6TT, 3000);
                }

                // If text within the EditText does not match, complete these instructions
                else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Not Quite. Try Again.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpWords6_1 = MediaPlayer.create(words6.this, R.raw.wrong);
                    mpWords6_1.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpWords6_1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpWords6_1.release();
                        }
                    });
                    wordUser6.setText("");
                    RelativeLayout rlWords6 = (RelativeLayout) findViewById(R.id.RLwords6);
                    rlWords6.setBackgroundColor(Color.RED);
                }

                // Log to confirm button has been pressed
                Log.i("answer6", "Words Answer6 Button Pressed");
            }

        });

        // Link to take user back to home activity
        ImageButton pageLinkhome = (ImageButton) findViewById(R.id.pageLinkhome);
        pageLinkhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(words6.this, home.class);
                startActivity(home);

                // Log to confirm button has been pressed
                Log.i("pageLinkhome", "Words 6 Home Button Pressed");
            }
        });
    }
}
