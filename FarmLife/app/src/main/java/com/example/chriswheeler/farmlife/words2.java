package com.example.chriswheeler.farmlife;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class words2 extends AppCompatActivity {

    EditText wordUser2;
    String strInputwords2 = "";
    Button wordAnswer2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_words2);

        // Setting EditText and Button within onCreate
        Button answer2 = (Button) findViewById(R.id.wordAnswer2);
        final EditText wordUser2 = (EditText) findViewById(R.id.wordUser2);

        // setting instructions for when button is clicked.
        answer2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // instruction to get text from the EditText
                String check = wordUser2.getText().toString();

                // If text within the EditText matches the below, complete these instructions
                // Code based on answer at http://stackoverflow.com/questions/24095450/compare-edittext-value-with-string
                if (check.equalsIgnoreCase("pig")) {
                    strInputwords2 = strInputwords2 + "," + check;
                    Toast toast = Toast.makeText(getApplicationContext(), "Correct", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpWords2 = MediaPlayer.create(words2.this, R.raw.correct);
                    mpWords2.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpWords2.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpWords2.release();
                        }
                    });
                    wordUser2.setText(strInputwords2);
                    wordUser2.setText("");
                    RelativeLayout rlWords2 = (RelativeLayout) findViewById(R.id.RLwords2);
                    rlWords2.setBackgroundColor(Color.GREEN);

                    // Timer to wait until above instructions are executed until activity changes.
                    TimerTask words2TT = new TimerTask() {
                        @Override
                        public void run() {

                            Intent words3 = new Intent(words2.this, words3.class);
                            startActivity(words3);

                        }
                    };

                    Timer words2Timer = new Timer();
                    words2Timer.schedule(words2TT, 3000);
                }

                // If text within the EditText does not match, complete these instructions
                else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Not Quite. Try Again.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpWords2_1 = MediaPlayer.create(words2.this, R.raw.wrong);
                    mpWords2_1.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpWords2_1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpWords2_1.release();
                        }
                    });
                    wordUser2.setText("");
                    RelativeLayout rlWords2 = (RelativeLayout) findViewById(R.id.RLwords2);
                    rlWords2.setBackgroundColor(Color.RED);
                }

                // Log to confirm button has been pressed
                Log.i("answer2", "Words Answer2 Button Pressed");
            }

        });

        // Link to take user back to home activity
        ImageButton pageLinkhome = (ImageButton) findViewById(R.id.pageLinkhome);
        pageLinkhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(words2.this, home.class);
                startActivity(home);

                // Log to confirm button has been pressed
                Log.i("pageLinkhome", "Words 2 Home Button Pressed");
            }
        });
    }
}
