package com.example.chriswheeler.farmlife;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class words3 extends AppCompatActivity {

    EditText wordUser3;
    String strInputwords3 = "";
    Button wordAnswer3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_words3);

        // Setting EditText and Button within onCreate
        Button answer3 = (Button) findViewById(R.id.wordAnswer3);
        final EditText wordUser3 = (EditText) findViewById(R.id.wordUser3);

        // setting instructions for when button is clicked.
        answer3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // instruction to get text from the EditText
                String check = wordUser3.getText().toString();

                // If text within the EditText matches the below, complete these instructions
                // Code based on answer at http://stackoverflow.com/questions/24095450/compare-edittext-value-with-string
                if (check.equalsIgnoreCase("sheep")) {
                    strInputwords3 = strInputwords3 + "," + check;
                    Toast toast = Toast.makeText(getApplicationContext(), "Correct", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpWords3 = MediaPlayer.create(words3.this, R.raw.correct);
                    mpWords3.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpWords3.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpWords3.release();
                        }
                    });
                    wordUser3.setText(strInputwords3);
                    wordUser3.setText("");
                    RelativeLayout rlWords3 = (RelativeLayout) findViewById(R.id.RLwords3);
                    rlWords3.setBackgroundColor(Color.GREEN);

                    // Timer to wait until above instructions are executed until activity changes.
                    TimerTask words3TT = new TimerTask() {
                        @Override
                        public void run() {

                            Intent words4 = new Intent(words3.this, words4.class);
                            startActivity(words4);

                        }
                    };

                    Timer words3Timer = new Timer();
                    words3Timer.schedule(words3TT, 3000);
                }

                // If text within the EditText does not match, complete these instructions
                else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Not Quite. Try Again.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpWords3_1 = MediaPlayer.create(words3.this, R.raw.wrong);
                    mpWords3_1.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpWords3_1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpWords3_1.release();
                        }
                    });
                    wordUser3.setText("");
                    RelativeLayout rlWords3 = (RelativeLayout) findViewById(R.id.RLwords3);
                    rlWords3.setBackgroundColor(Color.RED);
                }

                // Log to confirm button has been pressed
                Log.i("answer3", "Words Answer3 Button Pressed");
            }

        });

        // Link to take user back to home activity
        ImageButton pageLinkhome = (ImageButton) findViewById(R.id.pageLinkhome);
        pageLinkhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(words3.this, home.class);
                startActivity(home);

                // Log to confirm button has been pressed
                Log.i("pageLinkhome", "Words 3 Home Button Pressed");
            }
        });
    }
}
