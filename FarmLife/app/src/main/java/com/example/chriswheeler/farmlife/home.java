package com.example.chriswheeler.farmlife;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class home extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        // Home activity button to sounds activity
        ImageButton homeSounds = (ImageButton) findViewById(R.id.homeSounds);
        homeSounds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sounds1 = new Intent(home.this, sounds1.class);
                startActivity(sounds1);

                // Log to confirm button has been pressed
                Log.i("homeSounds", "Home Sounds Button Pressed");
            }
        });

        // Home activity button to numbers activity
        ImageButton homeNumbers = (ImageButton) findViewById(R.id.homeNumbers);
        homeNumbers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent numbers1 = new Intent(home.this, numbers1.class);
                startActivity(numbers1);

                // Log to confirm button has been pressed
                Log.i("homeNumbers","Home Numbers Button Pressed");
            }
        });

        // Home activity button to words activity
        ImageButton homeWords = (ImageButton) findViewById(R.id.homeWords);
        homeWords.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent words1 = new Intent(home.this, words1.class);
                startActivity(words1);

                // Log to confirm button has been pressed
                Log.i("homeWords", "Home Words Button Pressed");
            }
        });
    }
}
