package com.example.chriswheeler.farmlife;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class sounds1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sounds1);

        // COW - ImageButton that when clicked plays a sound file of the noise of the animal.
        // Code based on answer at http://stackoverflow.com/questions/2753943/how-to-play-sound-when-button-is-clicked-in-android
        ImageButton cow = (ImageButton) this.findViewById(R.id.cow);
        final MediaPlayer mpCow = MediaPlayer.create(this, R.raw.moo);
        cow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpCow.start();
                // On completion of sound file, this instruction releases the file to save memory and avoid errors
                mpCow.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        Log.v("onCompletion", "sound completed");
                        mpCow.release();
                    }
                });

                // Log to confirm button has been pressed
                Log.i("cow", "Sounds Cow Button Pressed");

            }

        });

        // PIG - ImageButton that when clicked plays a sound file of the noise of the animal.
        // Code based on answer at http://stackoverflow.com/questions/2753943/how-to-play-sound-when-button-is-clicked-in-android
        ImageButton pig = (ImageButton) this.findViewById(R.id.pig);
        final MediaPlayer mpPig = MediaPlayer.create(this, R.raw.oink);
        pig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpPig.start();
                // On completion of sound file, this instruction releases the file to save memory and avoid errors
                mpPig.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        Log.v("onCompletion", "sound completed");
                        mpPig.release();
                    }
                });

                // Log to confirm button has been pressed
                Log.i("pig", "Sounds Pig Button Pressed");

            }
        });

        // SHEEP - ImageButton that when clicked plays a sound file of the noise of the animal.
        // Code based on answer at http://stackoverflow.com/questions/2753943/how-to-play-sound-when-button-is-clicked-in-android
        ImageButton sheep = (ImageButton) this.findViewById(R.id.sheep);
        final MediaPlayer mpSheep = MediaPlayer.create(this, R.raw.baa);
        sheep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpSheep.start();
                // On completion of sound file, this instruction releases the file to save memory and avoid errors
                mpSheep.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        Log.v("onCompletion", "sound completed");
                        mpSheep.release();
                    }
                });

                // Log to confirm button has been pressed
                Log.i("sheep", "Sounds Sheep Button Pressed");

            }
        });

        // DOG - ImageButton that when clicked plays a sound file of the noise of the animal.
        // Code based on answer at http://stackoverflow.com/questions/2753943/how-to-play-sound-when-button-is-clicked-in-android
        ImageButton dog = (ImageButton) this.findViewById(R.id.dog);
        final MediaPlayer mpDog = MediaPlayer.create(this, R.raw.bark);
        dog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpDog.start();
                // On completion of sound file, this instruction releases the file to save memory and avoid errors
                mpDog.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        Log.v("onCompletion", "sound completed");
                        mpDog.release();
                    }
                });

                // Log to confirm button has been pressed
                Log.i("dog", "Sounds Dog Button Pressed");

            }
        });

        // CAT - ImageButton that when clicked plays a sound file of the noise of the animal.
        // Code based on answer at http://stackoverflow.com/questions/2753943/how-to-play-sound-when-button-is-clicked-in-android
        ImageButton cat = (ImageButton) this.findViewById(R.id.cat);
        final MediaPlayer mpCat = MediaPlayer.create(this, R.raw.meow);
        cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpCat.start();
                // On completion of sound file, this instruction releases the file to save memory and avoid errors
                mpCat.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        Log.v("onCompletion", "sound completed");
                        mpCat.release();
                    }
                });

                // Log to confirm button has been pressed
                Log.i("cat", "Sounds Cat Button Pressed");

            }
        });

        // LION - ImageButton that when clicked plays a sound file of the noise of the animal.
        // Code based on answer at http://stackoverflow.com/questions/2753943/how-to-play-sound-when-button-is-clicked-in-android
        ImageButton lion = (ImageButton) this.findViewById(R.id.lion);
        final MediaPlayer mpLion = MediaPlayer.create(this, R.raw.roar);
        lion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpLion.start();
                // On completion of sound file, this instruction releases the file to save memory and avoid errors
                mpLion.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        Log.v("onCompletion", "sound completed");
                        mpLion.release();
                    }
                });

                // Log to confirm button has been pressed
                Log.i("lion", "Sounds Lion Button Pressed");

            }
        });

        // MONKEY - ImageButton that when clicked plays a sound file of the noise of the animal.
        // Code based on answer at http://stackoverflow.com/questions/2753943/how-to-play-sound-when-button-is-clicked-in-android
        ImageButton monkey = (ImageButton) this.findViewById(R.id.monkey);
        final MediaPlayer mpMonkey = MediaPlayer.create(this, R.raw.monkeynoise);
        monkey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpMonkey.start();
                // On completion of sound file, this instruction releases the file to save memory and avoid errors
                mpMonkey.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        Log.v("onCompletion", "sound completed");
                        mpMonkey.release();
                    }
                });

                // Log to confirm button has been pressed
                Log.i("monkey", "Sounds Monkey Button Pressed");

            }
        });

        // RABBIT - ImageButton that when clicked plays a sound file of the noise of the animal.
        // Code based on answer at http://stackoverflow.com/questions/2753943/how-to-play-sound-when-button-is-clicked-in-android
        ImageButton rabbit = (ImageButton) this.findViewById(R.id.rabbit);
        final MediaPlayer mpRabbit = MediaPlayer.create(this, R.raw.purr);
        rabbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpRabbit.start();
                // On completion of sound file, this instruction releases the file to save memory and avoid errors
                mpRabbit.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        Log.v("onCompletion", "sound completed");
                        mpRabbit.release();
                    }
                });

                // Log to confirm button has been pressed
                Log.i("rabbit", "Sounds Rabbit Button Pressed");

            }
        });

        // MOUSE - ImageButton that when clicked plays a sound file of the noise of the animal.
        // Code based on answer at http://stackoverflow.com/questions/2753943/how-to-play-sound-when-button-is-clicked-in-android
        ImageButton mouse = (ImageButton) this.findViewById(R.id.mouse);
        final MediaPlayer mpMouse = MediaPlayer.create(this, R.raw.squeak);
        mouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mpMouse.start();
                // On completion of sound file, this instruction releases the file to save memory and avoid errors
                mpMouse.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        Log.v("onCompletion", "sound completed");
                        mpMouse.release();
                    }
                });

                // Log to confirm button has been pressed
                Log.i("mouse", "Sounds Mouse Button Pressed");

            }
        });

        // Link to take user back to home activity
        ImageButton pageLinkhome = (ImageButton) findViewById(R.id.pageLinkhome);
        pageLinkhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(sounds1.this, home.class);
                startActivity(home);

                // Log to confirm button has been pressed
                Log.i("pageLinkhome", "Sounds Home Button Pressed");
            }
        });
    }
}
