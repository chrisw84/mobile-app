package com.example.chriswheeler.farmlife;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class numbers6 extends AppCompatActivity {

    EditText userAnswer6;
    String strInput6 = "";
    Button answer6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numbers6);

        // Setting EditText and Button within onCreate
        Button answer6 = (Button) findViewById(R.id.answer6);
        final EditText userAnswer6 = (EditText) findViewById(R.id.userAnswer6);

        // setting instructions for when button is clicked.
        answer6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // instruction to get number from the EditText
                String check = userAnswer6.getText().toString();

                // If number within the EditText matches the below, complete these instructions
                // Code based on answer at http://stackoverflow.com/questions/66096660/compare-edittext-value-with-string
                if(check.equalsIgnoreCase("10"))
                {
                    strInput6 = strInput6+","+check;
                    Toast toast = Toast.makeText(getApplicationContext(), "Correct", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpNumbers6 = MediaPlayer.create(numbers6.this, R.raw.correct);
                    mpNumbers6.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpNumbers6.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpNumbers6.release();
                        }
                    });
                    userAnswer6.setText(strInput6);
                    userAnswer6.setText("");
                    RelativeLayout rlNumbers6 = (RelativeLayout) findViewById(R.id.RLnumbers6);
                    rlNumbers6.setBackgroundColor(Color.GREEN);

                    // Timer to wait until above instructions are executed until activity changes.
                    TimerTask numbers6TT = new TimerTask() {
                        @Override
                        public void run() {

                            Intent numbers7 = new Intent(numbers6.this, numbers7.class);
                            startActivity(numbers7);

                        }
                    };

                    Timer numbers6Timer = new Timer();
                    numbers6Timer.schedule(numbers6TT, 3000);

                }

                // If number within the EditText does not match, complete these instructions
                else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Not Quite. Try Again.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpNumbers6_1 = MediaPlayer.create(numbers6.this, R.raw.wrong);
                    mpNumbers6_1.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpNumbers6_1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpNumbers6_1.release();
                        }
                    });
                    userAnswer6.setText("");
                    RelativeLayout rlNumbers6 = (RelativeLayout) findViewById(R.id.RLnumbers6);
                    rlNumbers6.setBackgroundColor(Color.RED);
                }

                // Log to confirm button has been pressed
                Log.i("answer6", "Number Answer6 Button Pressed");
            }

        });

        // Link to take user back to home activity
        ImageButton pageLinkhome = (ImageButton) findViewById(R.id.pageLinkhome);
        pageLinkhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(numbers6.this, home.class);
                startActivity(home);

                // Log to confirm button has been pressed
                Log.i("pageLinkhome", "Number 6 Home Button Pressed");
            }
        });
    }
}
