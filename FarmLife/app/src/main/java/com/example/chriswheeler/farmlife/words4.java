package com.example.chriswheeler.farmlife;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class words4 extends AppCompatActivity {

    EditText wordUser4;
    String strInputwords4 = "";
    Button wordAnswer4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_words4);

        // Setting EditText and Button within onCreate
        Button answer4 = (Button) findViewById(R.id.wordAnswer4);
        final EditText wordUser4 = (EditText) findViewById(R.id.wordUser4);

        // setting instructions for when button is clicked.
        answer4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // instruction to get text from the EditText
                String check = wordUser4.getText().toString();

                // If text within the EditText matches the below, complete these instructions
                // Code based on answer at http://stackoverflow.com/questions/24095450/compare-edittext-value-with-string
                if (check.equalsIgnoreCase("dog")) {
                    strInputwords4 = strInputwords4 + "," + check;
                    Toast toast = Toast.makeText(getApplicationContext(), "Correct", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpWords4 = MediaPlayer.create(words4.this, R.raw.correct);
                    mpWords4.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpWords4.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpWords4.release();
                        }
                    });
                    wordUser4.setText(strInputwords4);
                    wordUser4.setText("");
                    RelativeLayout rlWords4 = (RelativeLayout) findViewById(R.id.RLwords4);
                    rlWords4.setBackgroundColor(Color.GREEN);

                    // Timer to wait until above instructions are executed until activity changes.
                    TimerTask words4TT = new TimerTask() {
                        @Override
                        public void run() {

                            Intent words5 = new Intent(words4.this, words5.class);
                            startActivity(words5);

                        }
                    };

                    Timer words4Timer = new Timer();
                    words4Timer.schedule(words4TT, 3000);
                }

                // If text within the EditText does not match, complete these instructions
                else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Not Quite. Try Again.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpWords4_1 = MediaPlayer.create(words4.this, R.raw.wrong);
                    mpWords4_1.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpWords4_1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpWords4_1.release();
                        }
                    });
                    wordUser4.setText("");
                    RelativeLayout rlWords4 = (RelativeLayout) findViewById(R.id.RLwords4);
                    rlWords4.setBackgroundColor(Color.RED);
                }

                // Log to confirm button has been pressed
                Log.i("answer4", "Words Answer4 Button Pressed");
            }

        });

        // Link to take user back to home activity
        ImageButton pageLinkhome = (ImageButton) findViewById(R.id.pageLinkhome);
        pageLinkhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(words4.this, home.class);
                startActivity(home);

                // Log to confirm button has been pressed
                Log.i("pageLinkhome", "Words 4 Home Button Pressed");
            }
        });
    }
}
