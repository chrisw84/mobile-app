package com.example.chriswheeler.farmlife;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class words1 extends AppCompatActivity {

    EditText wordUser;
    String strInputwords = "";
    Button wordAnswer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_words1);

        // Setting EditText and Button within onCreate
        Button answer = (Button) findViewById(R.id.wordAnswer);
        final EditText wordUser = (EditText) findViewById(R.id.wordUser);

        // setting instructions for when button is clicked.
        answer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // instruction to get text from the EditText
                String check = wordUser.getText().toString();

                // If text within the EditText matches the below, complete these instructions
                // Code based on answer at http://stackoverflow.com/questions/24095450/compare-edittext-value-with-string
                if (check.equalsIgnoreCase("cow")) {
                    strInputwords = strInputwords + "," + check;
                    Toast toast = Toast.makeText(getApplicationContext(), "Correct", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpWords1 = MediaPlayer.create(words1.this, R.raw.correct);
                    mpWords1.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpWords1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpWords1.release();
                        }
                    });
                    wordUser.setText(strInputwords);
                    wordUser.setText("");
                    RelativeLayout rlWords1 = (RelativeLayout) findViewById(R.id.RLwords1);
                    rlWords1.setBackgroundColor(Color.GREEN);

                    // Timer to wait until above instructions are executed until activity changes.
                    TimerTask words1TT = new TimerTask() {
                        @Override
                        public void run() {

                            Intent words2 = new Intent(words1.this, words2.class);
                            startActivity(words2);

                        }
                    };

                    Timer words1Timer = new Timer();
                    words1Timer.schedule(words1TT, 3000);
                }

                // If text within the EditText does not match, complete these instructions
                else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Not Quite. Try Again.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpWords1_1 = MediaPlayer.create(words1.this, R.raw.wrong);
                    mpWords1_1.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpWords1_1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpWords1_1.release();
                        }
                    });
                    wordUser.setText("");
                    RelativeLayout rlWords1 = (RelativeLayout) findViewById(R.id.RLwords1);
                    rlWords1.setBackgroundColor(Color.RED);
                }

                // Log to confirm button has been pressed
                Log.i("answer1", "Words Answer1 Button Pressed");
            }

        });

        // Link to take user back to home activity
        ImageButton pageLinkhome = (ImageButton) findViewById(R.id.pageLinkhome);
        pageLinkhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(words1.this, home.class);
                startActivity(home);

                // Log to confirm button has been pressed
                Log.i("pageLinkhome", "Words 1 Home Button Pressed");
            }
        });
    }
}
