package com.example.chriswheeler.farmlife;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class words8 extends AppCompatActivity {

    EditText wordUser8;
    String strInputwords8 = "";
    Button wordAnswer8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_words8);

        // Setting EditText and Button within onCreate
        Button answer8 = (Button) findViewById(R.id.wordAnswer8);
        final EditText wordUser8 = (EditText) findViewById(R.id.wordUser8);

        // setting instructions for when button is clicked.
        answer8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // instruction to get text from the EditText
                String check = wordUser8.getText().toString();

                // If text within the EditText matches the below, complete these instructions
                // Code based on answer at http://stackoverflow.com/questions/27097770/compare-edittext-value-with-string
                if (check.equalsIgnoreCase("rabbit")) {
                    strInputwords8 = strInputwords8 + "," + check;
                    Toast toast = Toast.makeText(getApplicationContext(), "Correct", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpWords8 = MediaPlayer.create(words8.this, R.raw.correct);
                    mpWords8.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpWords8.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpWords8.release();
                        }
                    });
                    wordUser8.setText(strInputwords8);
                    wordUser8.setText("");
                    RelativeLayout rlWords8 = (RelativeLayout) findViewById(R.id.RLwords8);
                    rlWords8.setBackgroundColor(Color.GREEN);

                    // Timer to wait until above instructions are executed until activity changes.
                    TimerTask words8TT = new TimerTask() {
                        @Override
                        public void run() {

                            Intent words9 = new Intent(words8.this, words9.class);
                            startActivity(words9);

                        }
                    };

                    Timer words8Timer = new Timer();
                    words8Timer.schedule(words8TT, 3000);
                }

                // If text within the EditText does not match, complete these instructions
                else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Not Quite. Try Again.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpWords8_1 = MediaPlayer.create(words8.this, R.raw.wrong);
                    mpWords8_1.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpWords8_1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpWords8_1.release();
                        }
                    });
                    wordUser8.setText("");
                    RelativeLayout rlWords8 = (RelativeLayout) findViewById(R.id.RLwords8);
                    rlWords8.setBackgroundColor(Color.RED);
                }

                // Log to confirm button has been pressed
                Log.i("answer8", "Words Answer8 Button Pressed");
            }

        });

        // Link to take user back to home activity
        ImageButton pageLinkhome = (ImageButton) findViewById(R.id.pageLinkhome);
        pageLinkhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(words8.this, home.class);
                startActivity(home);

                // Log to confirm button has been pressed
                Log.i("pageLinkhome", "Words 8 Home Button Pressed");
            }
        });
    }
}
