package com.example.chriswheeler.farmlife;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class words5 extends AppCompatActivity {

    EditText wordUser5;
    String strInputwords5 = "";
    Button wordAnswer5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_words5);

        // Setting EditText and Button within onCreate
        Button answer5 = (Button) findViewById(R.id.wordAnswer5);
        final EditText wordUser5 = (EditText) findViewById(R.id.wordUser5);

        // setting instructions for when button is clicked.
        answer5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // instruction to get text from the EditText
                String check = wordUser5.getText().toString();

                // If text within the EditText matches the below, complete these instructions
                // Code based on answer at http://stackoverflow.com/questions/25095550/compare-edittext-value-with-string
                if (check.equalsIgnoreCase("cat")) {
                    strInputwords5 = strInputwords5 + "," + check;
                    Toast toast = Toast.makeText(getApplicationContext(), "Correct", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpWords5 = MediaPlayer.create(words5.this, R.raw.correct);
                    mpWords5.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpWords5.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpWords5.release();
                        }
                    });
                    wordUser5.setText(strInputwords5);
                    wordUser5.setText("");
                    RelativeLayout rlWords5 = (RelativeLayout) findViewById(R.id.RLwords5);
                    rlWords5.setBackgroundColor(Color.GREEN);

                    // Timer to wait until above instructions are executed until activity changes.
                    TimerTask words5TT = new TimerTask() {
                        @Override
                        public void run() {

                            Intent words6 = new Intent(words5.this, words6.class);
                            startActivity(words6);

                        }
                    };

                    Timer words5Timer = new Timer();
                    words5Timer.schedule(words5TT, 3000);
                }

                // If text within the EditText does not match, complete these instructions
                else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Not Quite. Try Again.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpWords5_1 = MediaPlayer.create(words5.this, R.raw.wrong);
                    mpWords5_1.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpWords5_1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpWords5_1.release();
                        }
                    });
                    wordUser5.setText("");
                    RelativeLayout rlWords5 = (RelativeLayout) findViewById(R.id.RLwords5);
                    rlWords5.setBackgroundColor(Color.RED);
                }

                // Log to confirm button has been pressed
                Log.i("answer5", "Words Answer5 Button Pressed");
            }

        });

        // Link to take user back to home activity
        ImageButton pageLinkhome = (ImageButton) findViewById(R.id.pageLinkhome);
        pageLinkhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(words5.this, home.class);
                startActivity(home);

                // Log to confirm button has been pressed
                Log.i("pageLinkhome", "Words 5 Home Button Pressed");
            }
        });
    }
}
