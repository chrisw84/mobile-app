package com.example.chriswheeler.farmlife;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class words7 extends AppCompatActivity {

    EditText wordUser7;
    String strInputwords7 = "";
    Button wordAnswer7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_words7);

        // Setting EditText and Button within onCreate
        Button answer7 = (Button) findViewById(R.id.wordAnswer7);
        final EditText wordUser7 = (EditText) findViewById(R.id.wordUser7);

        // setting instructions for when button is clicked.
        answer7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // instruction to get text from the EditText
                String check = wordUser7.getText().toString();

                // If text within the EditText matches the below, complete these instructions
                // Code based on answer at http://stackoverflow.com/questions/27097770/compare-edittext-value-with-string
                if (check.equalsIgnoreCase("monkey")) {
                    strInputwords7 = strInputwords7 + "," + check;
                    Toast toast = Toast.makeText(getApplicationContext(), "Correct", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpWords7 = MediaPlayer.create(words7.this, R.raw.correct);
                    mpWords7.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpWords7.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpWords7.release();
                        }
                    });
                    wordUser7.setText(strInputwords7);
                    wordUser7.setText("");
                    RelativeLayout rlWords7 = (RelativeLayout) findViewById(R.id.RLwords7);
                    rlWords7.setBackgroundColor(Color.GREEN);

                    // Timer to wait until above instructions are executed until activity changes.
                    TimerTask words7TT = new TimerTask() {
                        @Override
                        public void run() {

                            Intent words8 = new Intent(words7.this, words8.class);
                            startActivity(words8);

                        }
                    };

                    Timer words7Timer = new Timer();
                    words7Timer.schedule(words7TT, 3000);
                }

                // If text within the EditText does not match, complete these instructions
                else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Not Quite. Try Again.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpWords7_1 = MediaPlayer.create(words7.this, R.raw.wrong);
                    mpWords7_1.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpWords7_1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpWords7_1.release();
                        }
                    });
                    wordUser7.setText("");
                    RelativeLayout rlWords7 = (RelativeLayout) findViewById(R.id.RLwords7);
                    rlWords7.setBackgroundColor(Color.RED);
                }

                // Log to confirm button has been pressed
                Log.i("answer7", "Words Answer7 Button Pressed");
            }

        });

        // Link to take user back to home activity
        ImageButton pageLinkhome = (ImageButton) findViewById(R.id.pageLinkhome);
        pageLinkhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(words7.this, home.class);
                startActivity(home);

                // Log to confirm button has been pressed
                Log.i("pageLinkhome", "Words 7 Home Button Pressed");
            }
        });
    }
}
