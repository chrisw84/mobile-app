package com.example.chriswheeler.farmlife;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class words9 extends AppCompatActivity {

    EditText wordUser9;
    String strInputwords9 = "";
    Button wordAnswer9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_words9);

        // Setting EditText and Button within onCreate
        Button answer9 = (Button) findViewById(R.id.wordAnswer9);
        final EditText wordUser9 = (EditText) findViewById(R.id.wordUser9);

        // setting instructions for when button is clicked.
        answer9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // instruction to get text from the EditText
                String check = wordUser9.getText().toString();

                // If text within the EditText matches the below, complete these instructions
                // Code based on answer at http://stackoverflow.com/questions/27097770/compare-edittext-value-with-string
                if (check.equalsIgnoreCase("mouse")) {
                    strInputwords9 = strInputwords9 + "," + check;
                    Toast toast = Toast.makeText(getApplicationContext(), "Correct", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpWords9 = MediaPlayer.create(words9.this, R.raw.correct);
                    mpWords9.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpWords9.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpWords9.release();
                        }
                    });
                    wordUser9.setText(strInputwords9);
                    wordUser9.setText("");
                    RelativeLayout rlWords9 = (RelativeLayout) findViewById(R.id.RLwords9);
                    rlWords9.setBackgroundColor(Color.GREEN);

                    // Timer to wait until above instructions are executed until activity changes.
                    TimerTask words9TT = new TimerTask() {
                        @Override
                        public void run() {

                            Intent home = new Intent(words9.this, home.class);
                            startActivity(home);

                        }
                    };

                    Timer words9Timer = new Timer();
                    words9Timer.schedule(words9TT, 3000);
                }

                // If text within the EditText does not match, complete these instructions
                else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Not Quite. Try Again.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpWords9_1 = MediaPlayer.create(words9.this, R.raw.wrong);
                    mpWords9_1.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpWords9_1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpWords9_1.release();
                        }
                    });
                    wordUser9.setText("");
                    RelativeLayout rlWords9 = (RelativeLayout) findViewById(R.id.RLwords9);
                    rlWords9.setBackgroundColor(Color.RED);
                }

                // Log to confirm button has been pressed
                Log.i("answer9", "Words Answer9 Button Pressed");
            }

        });

        // Link to take user back to home activity
        ImageButton pageLinkhome = (ImageButton) findViewById(R.id.pageLinkhome);
        pageLinkhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(words9.this, home.class);
                startActivity(home);

                // Log to confirm button has been pressed
                Log.i("pageLinkhome", "Words 9 Home Button Pressed");
            }
        });
    }
}
