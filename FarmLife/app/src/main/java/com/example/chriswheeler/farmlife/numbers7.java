package com.example.chriswheeler.farmlife;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class numbers7 extends AppCompatActivity {

    EditText userAnswer7;
    String strInput7 = "";
    Button answer7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numbers7);

        // Setting EditText and Button within onCreate
        Button answer7 = (Button) findViewById(R.id.answer7);
        final EditText userAnswer7 = (EditText) findViewById(R.id.userAnswer7);

        // setting instructions for when button is clicked.
        answer7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // instruction to get number from the EditText
                String check = userAnswer7.getText().toString();

                // If number within the EditText matches the below, complete these instructions
                // Code based on answer at http://stackoverflow.com/questions/77097770/compare-edittext-value-with-string
                if(check.equalsIgnoreCase("13"))
                {
                    strInput7 = strInput7+","+check;
                    Toast toast = Toast.makeText(getApplicationContext(), "Correct", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpNumbers7 = MediaPlayer.create(numbers7.this, R.raw.correct);
                    mpNumbers7.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpNumbers7.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpNumbers7.release();
                        }
                    });
                    userAnswer7.setText(strInput7);
                    userAnswer7.setText("");
                    RelativeLayout rlNumbers7 = (RelativeLayout) findViewById(R.id.RLnumbers7);
                    rlNumbers7.setBackgroundColor(Color.GREEN);

                    // Timer to wait until above instructions are executed until activity changes.
                    TimerTask numbers7TT = new TimerTask() {
                        @Override
                        public void run() {

                            Intent numbers8 = new Intent(numbers7.this, numbers8.class);
                            startActivity(numbers8);

                        }
                    };

                    Timer numbers7Timer = new Timer();
                    numbers7Timer.schedule(numbers7TT, 3000);

                }

                // If number within the EditText does not match, complete these instructions
                else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Not Quite. Try Again.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpNumbers7_1 = MediaPlayer.create(numbers7.this, R.raw.wrong);
                    mpNumbers7_1.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpNumbers7_1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpNumbers7_1.release();
                        }
                    });
                    userAnswer7.setText("");
                    RelativeLayout rlNumbers7 = (RelativeLayout) findViewById(R.id.RLnumbers7);
                    rlNumbers7.setBackgroundColor(Color.RED);
                }

                // Log to confirm button has been pressed
                Log.i("answer7", "Number Answer7 Button Pressed");
            }

        });

        // Link to take user back to home activity
        ImageButton pageLinkhome = (ImageButton) findViewById(R.id.pageLinkhome);
        pageLinkhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(numbers7.this, home.class);
                startActivity(home);

                // Log to confirm button has been pressed
                Log.i("pageLinkhome", "Number 7 Home Button Pressed");
            }
        });
    }
}
