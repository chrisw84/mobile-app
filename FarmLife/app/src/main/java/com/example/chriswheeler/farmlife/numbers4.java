package com.example.chriswheeler.farmlife;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class numbers4 extends AppCompatActivity {

    EditText userAnswer4;
    String strInput4 = "";
    Button answer4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numbers4);

        // Setting EditText and Button within onCreate
        Button answer4 = (Button) findViewById(R.id.answer4);
        final EditText userAnswer4 = (EditText) findViewById(R.id.userAnswer4);

        // setting instructions for when button is clicked.
        answer4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // instruction to get number from the EditText
                String check = userAnswer4.getText().toString();

                // If number within the EditText matches the below, complete these instructions
                // Code based on answer at http://stackoverflow.com/questions/44095450/compare-edittext-value-with-string
                if(check.equalsIgnoreCase("6"))
                {
                    strInput4 = strInput4+","+check;
                    Toast toast = Toast.makeText(getApplicationContext(), "Correct", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpNumbers4 = MediaPlayer.create(numbers4.this, R.raw.correct);
                    mpNumbers4.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpNumbers4.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpNumbers4.release();
                        }
                    });
                    userAnswer4.setText(strInput4);
                    userAnswer4.setText("");
                    RelativeLayout rlNumbers4 = (RelativeLayout) findViewById(R.id.RLnumbers4);
                    rlNumbers4.setBackgroundColor(Color.GREEN);

                    // Timer to wait until above instructions are executed until activity changes.
                    TimerTask numbers4TT = new TimerTask() {
                        @Override
                        public void run() {

                            Intent numbers5 = new Intent(numbers4.this, numbers5.class);
                            startActivity(numbers5);

                        }
                    };

                    Timer numbers4Timer = new Timer();
                    numbers4Timer.schedule(numbers4TT, 3000);

                }

                // If number within the EditText does not match, complete these instructions
                else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Not Quite. Try Again.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpNumbers4_1 = MediaPlayer.create(numbers4.this, R.raw.wrong);
                    mpNumbers4_1.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpNumbers4_1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpNumbers4_1.release();
                        }
                    });
                    userAnswer4.setText("");
                    RelativeLayout rlNumbers4 = (RelativeLayout) findViewById(R.id.RLnumbers4);
                    rlNumbers4.setBackgroundColor(Color.RED);
                }

                // Log to confirm button has been pressed
                Log.i("answer4", "Number Answer4 Button Pressed");
            }

        });

        // Link to take user back to home activity
        ImageButton pageLinkhome = (ImageButton) findViewById(R.id.pageLinkhome);
        pageLinkhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(numbers4.this, home.class);
                startActivity(home);

                // Log to confirm button has been pressed
                Log.i("pageLinkhome", "Number 4 Home Button Pressed");
            }
        });
    }
}
