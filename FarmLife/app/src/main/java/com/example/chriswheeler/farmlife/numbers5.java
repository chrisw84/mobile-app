package com.example.chriswheeler.farmlife;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class numbers5 extends AppCompatActivity {

    EditText userAnswer5;
    String strInput5 = "";
    Button answer5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numbers5);

        // Setting EditText and Button within onCreate
        Button answer5 = (Button) findViewById(R.id.answer5);
        final EditText userAnswer5 = (EditText) findViewById(R.id.userAnswer5);

        // setting instructions for when button is clicked.
        answer5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // instruction to get number from the EditText
                String check = userAnswer5.getText().toString();

                // If number within the EditText matches the below, complete these instructions
                // Code based on answer at http://stackoverflow.com/questions/55095550/compare-edittext-value-with-string
                if(check.equalsIgnoreCase("4"))
                {
                    strInput5 = strInput5+","+check;
                    Toast toast = Toast.makeText(getApplicationContext(), "Correct", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpNumbers5 = MediaPlayer.create(numbers5.this, R.raw.correct);
                    mpNumbers5.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpNumbers5.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpNumbers5.release();
                        }
                    });
                    userAnswer5.setText(strInput5);
                    userAnswer5.setText("");
                    RelativeLayout rlNumbers5 = (RelativeLayout) findViewById(R.id.RLnumbers5);
                    rlNumbers5.setBackgroundColor(Color.GREEN);

                    // Timer to wait until above instructions are executed until activity changes.
                    TimerTask numbers5TT = new TimerTask() {
                        @Override
                        public void run() {

                            Intent numbers6 = new Intent(numbers5.this, numbers6.class);
                            startActivity(numbers6);

                        }
                    };

                    Timer numbers5Timer = new Timer();
                    numbers5Timer.schedule(numbers5TT, 3000);

                }

                // If number within the EditText does not match, complete these instructions
                else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Not Quite. Try Again.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
                    final MediaPlayer mpNumbers5_1 = MediaPlayer.create(numbers5.this, R.raw.wrong);
                    mpNumbers5_1.start();
                    // On completion of sound file, this instruction releases the file to save memory and avoid errors
                    mpNumbers5_1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            Log.v("onCompletion", "sound completed");
                            mpNumbers5_1.release();
                        }
                    });
                    userAnswer5.setText("");
                    RelativeLayout rlNumbers5 = (RelativeLayout) findViewById(R.id.RLnumbers5);
                    rlNumbers5.setBackgroundColor(Color.RED);
                }

                // Log to confirm button has been pressed
                Log.i("answer5", "Number Answer5 Button Pressed");
            }

        });

        // Link to take user back to home activity
        ImageButton pageLinkhome = (ImageButton) findViewById(R.id.pageLinkhome);
        pageLinkhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(numbers5.this, home.class);
                startActivity(home);

                // Log to confirm button has been pressed
                Log.i("pageLinkhome", "Number 5 Home Button Pressed");
            }
        });
    }
}
